#### S3 ####
variable "aws-region" {
  type = "string"
  description = "AWS Region"
}
variable "bucket-name" {
  type = "string"
  description = "Enter bucket name."
}
variable "bucket-owner" {
  type = "string"
  description = "Enter the Tag value for owner."
}
variable "bucket-env" {
  type = "string"
  description = "Enter the tag value for Environment."
}
