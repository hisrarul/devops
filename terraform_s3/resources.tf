provider "aws" {
    region = "${var.aws-region}"
}
resource "aws_s3_bucket" "s3bucket" {
  bucket = "${var.bucket-name}"
  acl    = "private"
  tags {
      Owner       = "${var.bucket-owner}"
      Environment = "${var.bucket-env}"
  }
  versioning {
      enabled = true
  }
}
