output "Bucket Name" {
  value = "${aws_s3_bucket.s3bucket.bucket}"
}
output "Bucket" {
  value = "${aws_s3_bucket.s3bucket.id}"
}

