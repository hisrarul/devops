## Run Jenkins on Container

### Requirement
### Create a pipeline which will allow you to provision cloudformation stack on aws

1. Create a Dockerfile

2. Build an image using Dockerfile
    
    **docker build -it jenkins/jenkins:israrul .(dot)**

3. Run the container using new built image

    **docker run -e AWS_ACCESS_KEY_ID=<access_key> -e AWS_SECRET_ACCESS_KEY=<secret_key> -it jenkin/jenkins:israrul /bin/bash**

Here, we also trying to pass access key and secret key in environment variable so we can run aws cli on Jenkins Job