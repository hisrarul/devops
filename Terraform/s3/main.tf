  resource "aws_s3_bucket" "destination" {
  count = "${var.enabledestinationbucket ? 1 : 0}"
  bucket = "${var.destination_bucket}"
  region = "${var.destination_region}"

  versioning {
    enabled = true
  }
  }