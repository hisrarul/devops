variable "highLevelOU" {
  default = "MIB"
}

variable "ChildOU1" {
  default = "Allen"
}

variable "ChildOU2" {
  default = "Infra"
}

variable "ChildOU3" {
  default = "Environment"
}

variable "whitelistSCP" {
  default = "whitelist_policy_OU"
}