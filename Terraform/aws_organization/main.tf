# Create Organization (Master account gets created by default - when an Organization is created)
resource "aws_organizations_organization" "org" {
  feature_set = "ALL"
}

# Create High Level OU = FR
resource "aws_organizations_organizational_unit" "MIB" {
  name      = "${var.highLevelOU}"
  parent_id = aws_organizations_organization.org.roots[0].id
}

# Create a child OU = "Infra" for Parent OU = "MIB"
resource "aws_organizations_organizational_unit" "Infra" {
  name      = "${var.ChildOU1}"
  parent_id = aws_organizations_organizational_unit.MIB.id
}

# Create a child OU = "Allen" for Parent OU = "MIB"
resource "aws_organizations_organizational_unit" "Allen" {
  name      = "${var.ChildOU2}"
  parent_id = aws_organizations_organizational_unit.MIB.id
}

# Create a child OU = "Environment" for Parent OU = "MIB"
resource "aws_organizations_organizational_unit" "Environment" {
  name      = "${var.ChildOU3}"
  parent_id = aws_organizations_organizational_unit.MIB.id
}
#####

resource "aws_organizations_policy" "whitelist_policy_OU" {
  name = "${var.whitelistSCP}"

  content = <<CONTENT
{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Effect": "Allow",
			"Action": [
				"athena:*",
				"application-autoscaling:*",
				"autoscaling:*",
				"autoscaling-plans:*",
				"billing:*",
				"budgetservice:*",
				"config:*",
				"certificatemanager:*",
				"certificatemanagerprivatecertificateauthority:*",
				"cloudformation:*",
				"cloudtrail:*",
				"cloudwatch:*",
				"applicationinsights:*",
				"events:*",
				"dlm:*",
				"directconnect:*",
				"dynamodb:*",
				"datasync:*",
				"dax:*",
				"dms:*",
				"ds:*",
				"ecr:*",
				"ec2:*",
				"elasticfilesystem:*",
				"eks:*",
				"elasticloadbalancing:*",
				"elasticloadbalancingv2:*",
				"elasticmapreduce:*",
				"firehose:*",
				"fsx:*",
				"glacier:*",
				"glue:*",
				"guardduty:*",
				"health:*",
				"iam:*",
				"kinesis:*",
				"kms:*",
				"lamda:*",
				"logs:*",
				"rds:*",
				"redshift:*",
				"ram:*",
				"resource-groups:*",
				"resourcegroupstaggingapi:*",
				"route53:*",
				"route53resolver:*",
				"route53domains:*",
				"s3:*",
				"securityhub:*",
				"sso:*",
				"stepfunctions:*",
				"support:*",
				"s3api:*",
				"s3control:*",
				"secretmanager:*",
				"sns:*",
				"sqs:*",
				"ssm:*",
				"sts:*",
				"trustadvisor:*",
				"organizations:*"
			],
			"Resource": [
				"*"
			]
		}
	]
}
CONTENT
}


resource "aws_organizations_policy_attachment" "unit" {
  policy_id = "${aws_organizations_policy.whitelist_policy_OU.id}"
  target_id = aws_organizations_organizational_unit.MIB.id
}