variable "ami_id" {
  default = "ami-0b898040803850657"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "subnet_id" {
  default = ""
}

variable "ec2_count" {
  default = ""
}

variable "aws_region" {
  default = []
}

