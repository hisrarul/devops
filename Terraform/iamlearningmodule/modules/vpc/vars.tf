variable "vpc_cidr" {
  default = "30.0.0.0/16"
}

variable "tenancy" {
  default = "dedicated"
}

variable "subnet_cidr" {
  default = "30.0.0.0/17"
}

variable "aws_region" {
  default = []
}
