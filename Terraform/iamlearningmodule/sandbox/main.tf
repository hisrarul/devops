provider "aws" {
  region = "us-east-1"
}
module "my_vpc" {
  source = "../modules/vpc"
  vpc_cidr = "40.0.0.0/16"
  tenancy = "default"
  subnet_cidr = "40.0.0.0/17"

}

module "my_ec2" {
  source = "../modules/ec2"
  ec2_count = 1
  instance_type = "t2.micro"
  subnet_id = "${module.my_vpc.subnet_id}"
}
