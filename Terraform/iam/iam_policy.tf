resource "aws_iam_policy" "policy" {
  name        = "patchinstancerollbackpolicy"
  path        = "/"
  description = "patch policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ec2:Describe*",
        "ec2:StopInstances",
        "ec2:StartInstances",
        "ssm:ListCommands",
        "ssm:ListComplianceItems",
        "s3:PutObject",
        "cloudwatch:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}