resource "aws_iam_role" "patchinstancerole" {
    name = "PatchInstanceWithRollbackStack"
    description = "IAM role for patching"
    path = "/"

    assume_role_policy = <<-EOF
    {
    "Version": "2012-10-17",
    "Statement": [
        {
        "Effect": "Allow",
        "Principal": {
            "Service": "lambda.amazonaws.com"
        },
        "Action": "sts:AssumeRole"
        }
      ]
    }
    EOF
}

resource "aws_iam_role_policy_attachment" "patchinstanceattachment" {
    role = "${aws_iam_role.patchinstancerole.name}"
    policy_arn = "${aws_iam_policy.policy.arn}"
}