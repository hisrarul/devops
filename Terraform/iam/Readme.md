##Import the existing resource commands

terraform import aws_iam_role.patchinstancerole PatchInstanceWithRollbackStack

terraform show terraform.tfstate

terraform import aws_iam_policy.policy arn:aws:iam::XXXXXXXXXXX:policy/patchinstancerollbackpolicy

terraform import aws_iam_role_policy_attachment.patchinstanceattachment PatchInstanceWithRollbackStack/arn:aws:iam::XXXXXXXXXXX:policy/patchinstancerollbackpolicy